package se.prv.database.db2migrering.domain;

import java.io.Serializable;

public class LegacyColumnMeta implements Serializable {

	private static final long serialVersionUID = -3643957995930330914L;
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	private String name;
	private String coltype;
	private int length;
	private boolean nulls;
	
	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setColtype(String coltype) {
		this.coltype = coltype.trim();
	}
	
	public String getColtype() {
		return coltype;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getLength() {
		return length;
	}

	public void setNulls(boolean nulls) {
		this.nulls = nulls;
	}
	
	public boolean isNulls() {
		return nulls;
	}

	public String toString() {
		return "LegacyColumnMeta{name="+name+", coltype="+coltype+", length="+length+", nulls="+nulls+"}";
	}

}
