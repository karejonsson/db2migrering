package se.prv.database.db2migrering.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LegacyTableMeta implements Serializable {
	
	private static final long serialVersionUID = -3985939887054506583L;
	
	private String name;

	public LegacyTableMeta(String name) {
		this.name = name;
	}
	
	private List<LegacyColumnMeta> columns = new ArrayList<LegacyColumnMeta>();

	public void addColumn(LegacyColumnMeta lcm) {
		columns.add(lcm);
	}
	
	public int getColCount() {
		return columns.size();
	}	
	
	public List<LegacyColumnMeta> getColuns() {
		return columns;
	}
	
	public String toString() {
		return "LegacyTableMeta{name="+name+", columns="+Arrays.toString(columns.toArray())+"}";
	}
	
}
