package se.prv.database.db2migrering;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import se.prv.database.db2migrering.connectivity.FindLegacyTables;
import se.prv.database.db2migrering.domain.LegacyTableMeta;

public class GetRawStructure {
    
	public static void main(String[] args) throws IOException {
		List<String> tablenames = FindLegacyTables.findTablenamesBySchema("ADBPROD");
		System.out.println("Fann "+tablenames.size()+" namn");
		int colCount = 0;
		List<LegacyTableMeta> tablesMeta = new ArrayList<LegacyTableMeta>();
		for(String tablename : tablenames) {
			LegacyTableMeta ltm = FindLegacyTables.findTableBySchema("ADBPROD", tablename);
			tablesMeta.add(ltm);
			colCount += ltm.getColCount();
			System.out.println(ltm.toString());
		}
		System.out.println("Antal kolumner "+colCount);
		
		FileOutputStream fout = new FileOutputStream("DB2_tables.ser");
		ObjectOutputStream oos = new ObjectOutputStream(fout);
		oos.writeObject(tablesMeta);
		oos.flush();
		oos.close();
		fout.flush();
		fout.close();
	}

}
