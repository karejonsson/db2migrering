package se.prv.database.db2migrering.connectivity;

import general.reuse.properties.HarddriveProperties;

public class InstallationProperties {

	private static final String pathToConfigFile_property = "prv.db2migrering.configfile";
	private static final String pathToConfigFile_default = "/etc/prvconfig/db2migrering.properties";

	private static HarddriveProperties hp = new HarddriveProperties(pathToConfigFile_property, pathToConfigFile_default);
	 
	public static String getString(String propertyname) throws Exception {
		return hp.getString(propertyname);
	}

	public static String[] getStringArray(String propertyname) throws Exception {
		return hp.getStringArray(propertyname);
	}

	public static boolean getBoolean(String propertyname, final boolean defaultValue) {
		return hp.getBoolean(propertyname, defaultValue);
	}
	
	public static String getString(String propertyname, final String defaultValue) {
		return hp.getString(propertyname, defaultValue);
	}
	
	public static Integer getInt(String propertyname, final Integer defaultValue) {
		return hp.getCLPProperty(propertyname, defaultValue);
	}
	
	public static final String jdcb_driver_class_property = "prv.db2migrering.jdbc.driverclass";
	public static final String jdcb_url_property = "prv.db2migrering.jdbc.url";
	public static final String jdcb_username_property = "prv.db2migrering.jdbc.username";
	public static final String jdcb_password_property = "prv.db2migrering.jdcb.password";
	
	public static void main(String args[]) throws Exception {
		System.out.println("Driver klass "+getString(jdcb_driver_class_property, "inte rätt"));
	}

}
