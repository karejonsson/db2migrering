package se.prv.database.db2migrering.connectivity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.database.db2migrering.domain.LegacyColumnMeta;
import se.prv.database.db2migrering.domain.LegacyTableMeta; 

public class FindLegacyTables {

	public static List<String> findTablenamesBySchema(String Schema) {
		List<String> out = new ArrayList<String>();
		JDBCConnectionPool pool = null;	
		Connection conn = null;		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection();
			stmt = conn.prepareStatement(
					"select NAME from sysibm.systables where CREATOR = ? and type = 'T';");
			stmt.setString(1, Schema);
			rs = stmt.executeQuery();
			while(rs.next()) {
				out.add(rs.getString(1));
			}
			return out;
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return null;
	}	

	public static LegacyTableMeta findTableBySchema(String Schema, String tablename) {
		JDBCConnectionPool pool = null;	
		LegacyTableMeta out = null;
		Connection conn = null;		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection();
			stmt = conn.prepareStatement(
					"select NAME, COLTYPE, LENGTH, NULLS from sysibm.syscolumns where TBCREATOR = ? and TBNAME = ?;");
			stmt.setString(1, Schema);
			stmt.setString(2, tablename);
			rs = stmt.executeQuery(); 
			out = new LegacyTableMeta(tablename);
			while(rs.next()) {
				LegacyColumnMeta columnMeta = new LegacyColumnMeta();
				columnMeta.setName(rs.getString(1));
				columnMeta.setColtype(rs.getString(2));
				columnMeta.setLength(rs.getInt(3));
				columnMeta.setNulls(rs.getBoolean(4));
				out.addColumn(columnMeta);
			}
			return out;
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return null;
	}	

}
